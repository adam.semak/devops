Branch: feature/devops-test
PR: feature/devops-test -> master

1: Pipeline configuration under gitlab-ci.yml
   Stages:
   - test
   - build_app
   - build_docker
   - deploy
   
2: Gitlab: environment created with protection
3: Gitlab: protection set on branch
4: Gitlab: variable with docker registry email, user and password has been created
5: deployment.yml file was created - deployment for kubernetes
6: minikube image is used for demo deployment